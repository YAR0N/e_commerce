package app.repositories;

import models.Order;


public abstract class OrderRepository implements IRepository {

    public abstract void create(Order order);

    public abstract void update(Order order);

    public abstract void delete(Order order);

    public abstract Order read(int id);

}
