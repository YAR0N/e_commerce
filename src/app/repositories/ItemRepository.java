package app.repositories;

import models.Item;


public abstract class ItemRepository implements IRepository {

    public abstract void create(Item order);

    public abstract void update(Item order);

    public abstract void delete(Item order);

    public abstract Item read(int id);


}
