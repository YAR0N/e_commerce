package app.services;

import models.Invoice;


public interface IPaymentService {

    void process(Invoice invoice);

}
