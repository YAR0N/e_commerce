package app.services;

import models.Order;


public interface IOrderService {

    void process(Order order);

}
