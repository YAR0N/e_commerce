package app.services;

import app.repositories.OrderRepository;
import models.Order;


public class OrderService implements IOrderService {

    private OrderRepository orderRepository;

    private IPaymentService paymentService;

    public OrderService(OrderRepository orderRepository, IPaymentService paymentService) {
        this.orderRepository = orderRepository;
        this.paymentService = paymentService;
    }

    @Override
    public void process(Order order) {
        this.orderRepository.create(order);
        this.paymentService.process(order.getInvoice());
    }
}
