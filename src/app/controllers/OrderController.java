package app.controllers;

import app.repositories.ItemRepository;
import app.repositories.OrderRepository;
import app.services.ChequePaymentService;
import app.services.IOrderService;
import app.services.IPaymentService;
import app.services.OrderService;
import db.ItemRepositoryDB;
import db.OrderRepositoryDB;
import models.Customer;
import models.Item;
import models.Order;
import models.OrderFactory;
import stripeIntegration.StripePaymentService;

import java.util.ArrayList;
import java.util.List;


public class OrderController implements IController {

    private ItemRepository itemRepository = new ItemRepositoryDB();
    private OrderRepository orderRepository = new OrderRepositoryDB();


    public void makeOder(int[] items_ids, String customer_name, String payment_type){

        List<Item> items = new ArrayList<>();

        for (int item_id: items_ids){
            items.add(itemRepository.read(item_id));
        }

        IPaymentService paymentService;

        if (payment_type.equals("cheque")){
            paymentService = new ChequePaymentService();
        } else {
            paymentService = new StripePaymentService();
        }

        OrderFactory orderFactory = new OrderFactory();

        Order order = orderFactory.makeOrder(new Customer(customer_name), items);

        OrderService orderService = new OrderService(orderRepository, paymentService);
        orderService.process(order);

    }

}
