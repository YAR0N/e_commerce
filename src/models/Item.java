package models;


public class Item implements IModel {

    private String name;
    private String description;
    private float price;
    private int id;


    public Item(String name, String description, float price, int id) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.id = id;
    }


}
