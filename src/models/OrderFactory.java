package models;

import java.util.List;


public class OrderFactory {

    public Order makeOrder(Customer customer, List<Item> items){
        Invoice invoice = new Invoice(orderSum(items));

        int id = 0;  //Randomly generated id

        return new Order(items, customer, invoice, id);
    }

    private float orderSum(List<Item> items){
//        returns sum of all items
        return 0;
    }
}
