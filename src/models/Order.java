package models;

import java.util.List;


public class Order implements IModel {

    private int id;

    private List<Item> items;
    private Customer customer;
    private Invoice invoice;

    public Order(List<Item> items, Customer customer, Invoice invoice, int id) {
        this.items = items;
        this.customer = customer;

        this.invoice = invoice;

        this.id = id;
    }

    public Invoice getInvoice() {
        return invoice;
    }
}
