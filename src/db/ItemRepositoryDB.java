package db;

import app.repositories.ItemRepository;
import models.Item;


public class ItemRepositoryDB extends ItemRepository {

    @Override
    public void create(Item order) {

    }

    @Override
    public void update(Item order) {

    }

    @Override
    public void delete(Item order) {

    }

    @Override
    public Item read(int id) {
        return new Item(null, null, 0, id);
    }
}
